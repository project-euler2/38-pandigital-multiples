import time
start_time = time.time()

def find_pandigital_mult(pandigital):
    biggest_pandigital = 0
    for i in range(9123,9876):
        if sorted(map(int,list((str(i) + str(i*2))))) == pandigital:
            if int(str(i)+str(i*2)) > biggest_pandigital:
                biggest_pandigital = int(str(i)+str(i*2))
    return biggest_pandigital

print(find_pandigital_mult([1,2,3,4,5,6,7,8,9]))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )